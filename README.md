# How to Design GNOME Wallpapers

![GNOME_Wallpapers](/uploads/152f76568616dba96cd3c2443e072449/GNOME_Wallpapers.png)

Creating **GNOME wallpapers** (light and dark) with Blender 3D can be a fun and rewarding project, allowing you to express your creativity while showcasing your love for Linux and 3D design skills. This is not the official wallpaper site, If you want to go to the official website click into [GNOME Backgrounds](https://gitlab.gnome.org/GNOME/gnome-backgrounds)

All these works are designed using Shader and Geometry Nodes under Blender.

Here's a step-by-step guide to help you get started:

Set Up Blender: If you haven't already, download and install Blender from **Flathub**
Blender is a free and open-source 3D creation suite that's perfect for creating wallpapers.

## Download Blender from Flathub:
```
flatpak install flathub org.blender.Blender
```
Run Blender:
```
flatpak run org.blender.Blender
```
## Setting Up Blender
### Understand Wallpaper Dimensions:
Determine the resolution and aspect ratio for your wallpaper. Common resolutions include: 

- 3840x2160 (4K)
- 2560x1440 (QHD)
- 1920x1080 (Full HD)

Knowing your **target resolution** **1920x1080** will help you set up your **Blender Scene Properly**.

### Add Node Wrangler
This add-on gives you several tools that help you work with nodes quickly and efficiently. Many functions work for both the Compositor and shader nodes, and some functions bring features already in the Compositor to the shader nodes as well:

![Node_Wrangler](/uploads/a2d500d20fda9ebc862e9f0123db5017/Node_Wrangler.png)

### Switch to Cycles Render Engine: 
Blender offers two main rendering engines - Cycles and Eevee. 

In thid tutorial we are gonna use:

```
Cycles Render Engine
```
### Tips ###
Add **Mapping** and **Texture Coordinate**

```
Ctrl + t
```
